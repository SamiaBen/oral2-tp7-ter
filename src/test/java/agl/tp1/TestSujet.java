package agl.tp1;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestSujet {
	
	public Sujet sujet1;
	public Sujet sujet2;
	public Sujet sujet3; // constructeur sans titre
	
	@BeforeEach
	public void setUp(){
		sujet1 = new Sujet("titre1");
		sujet2 = new Sujet("titre2");
		sujet3 = new Sujet();
	}
	
	@Test
	void testGetTitre() {
		assertEquals("titre1",sujet1.getTitre());
	}
	
	@Test
	void testSetTitre() {
		sujet3.setTitre("ajoutTitre3");
		assertEquals("ajoutTitre3",sujet3.getTitre());
	}
	
	@Test
	void testGetId() { //Teste aussi l'incrémentation automatique de index
		assertEquals(sujet1.getId()+1,sujet2.getId());
	}
	
	@Test
	void testToString() {
		assertEquals(sujet1.getTitre()+" (id:"+sujet1.getId()+")",sujet1.toString());
	}

}
