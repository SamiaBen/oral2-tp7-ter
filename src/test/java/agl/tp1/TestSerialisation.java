package agl.tp1;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;



class TestSerialisation {
	
	public GestionTER TER1;

	
	@BeforeEach
	public void setUp() throws CantAddVoeu {
		TER1 = new GestionTER();
		
		//ajouter groupes + sujets
		TER1.addGroupe("groupe A");
        TER1.addGroupe("groupe B");
        TER1.addGroupe("groupe C");
        TER1.addGroupe("groupe D");
        TER1.addGroupe("groupe E");
        TER1.addGroupe("groupe F");

        TER1.addSujet("sujet 1");
        TER1.addSujet("sujet 2");
        TER1.addSujet("sujet 3");
        TER1.addSujet("sujet 4");
        TER1.addSujet("sujet 5");
        TER1.addSujet("sujet 6");

        ArrayList<Sujet> sujets = TER1.getSujets();

        //affecter aléatoirement des voeux aux groupes
        Groupe g;
        int nbSujets = sujets.size();
        for(int i=0; i<TER1.getNbGroupe(); i++) {
          g = TER1.getGroupe(i);
          int ordre = 1;
          for(int j=1; j<nbSujets; j++) {
            int index = (int)(Math.random() * nbSujets);
            while (g.isSujetInVoeu(sujets.get(index))) {
            	index = (int)(Math.random() * nbSujets);
            }
            Sujet s = sujets.get(index);
            if(g.addVoeu(ordre, s)) {
              ordre++;
            }
          }
        }
	}
	
	@Test
	void testSerialisationTER() throws Exception {
		String string1 = TER1.toString();
		
		 //serialiser données groupe et voeu
		TER1.serializeGroupe();
        TER1.serializeSujet();
        
        //reinitialiser l'app
        TER1 = new GestionTER();
        
        //app.importSujet(sujetSer);
        TER1.importGroupe();
        TER1.importSujet();
        
        assertEquals(string1,TER1.toString());
        //On vérifie que le toString() avant la sérialisation est égale
        //au toString() après la sérialisation pour être sûr que TER1
        //n'a pas changé et que la sauvegarde dans le .JSON est correcte.
	}

}
