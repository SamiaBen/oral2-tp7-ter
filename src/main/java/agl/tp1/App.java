package agl.tp1;

import java.lang.management.OperatingSystemMXBean;
import java.util.Random;

//Version 1.0

public class App
{
    public static void main( String[] args )
    {
        GestionTER gestionTerAleatoire = AffichageAffectation.generationAleatoireGestionTer(20);
        System.out.println(gestionTerAleatoire);
        AffichageAffectation.affectationsVersHtml(gestionTerAleatoire);
    }
}

